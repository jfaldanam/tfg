Dataset containing correlations between several vegetation indices (NDVI and MOISTURE) and the temperature at the time of the capture.

![](https://opendata.khaos.uma.es/dataset/ee825578-3d61-4f8a-b23e-b450da1898fd/resource/1e860ec0-4653-4860-ab39-ed919ced9d92/download/ndvitemp.png)

![](https://opendata.khaos.uma.es/dataset/ee825578-3d61-4f8a-b23e-b450da1898fd/resource/ff20fd0d-05a4-4f45-9415-eb21a01763b0/download/moisturetemp.png)


