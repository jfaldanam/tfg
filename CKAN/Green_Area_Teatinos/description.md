Dataset containing the area in hectares (ha) of green zone in the campus of Teatinos of the University of Málaga, as well as several Vegetative Indices and meteorological data taken from [AEMET Open Data portal](https://opendata.aemet.es/centrodedescargas/inicio)

[![](http://opendata.khaos.uma.es/dataset/ee825578-3d61-4f8a-b23e-b450da1898fd/resource/870330a4-7f65-4a8b-b7c7-19d583b7031a/download/greenareateatinos_small.png)](http://opendata.khaos.uma.es/dataset/ee825578-3d61-4f8a-b23e-b450da1898fd/resource/7e805f61-1b2c-4826-8105-59ccb8c3785c/download/greenareateatinos.png)

[![](http://opendata.khaos.uma.es/dataset/ee825578-3d61-4f8a-b23e-b450da1898fd/resource/5a5d9183-6781-4a1b-bf98-dd323e95c6f3/download/temporalseriesteatinos.png)](http://opendata.khaos.uma.es/dataset/ee825578-3d61-4f8a-b23e-b450da1898fd/resource/5a5d9183-6781-4a1b-bf98-dd323e95c6f3/download/temporalseriesteatinos.png)

