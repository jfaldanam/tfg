Ontology containing both the terminological component  (TBox) and the assertion component (ABox) of the earth observation ontology.

This ontology consolidates and integrates data and metadata from satellite products, like [Sentinel-2](https://scihub.copernicus.eu/dhus) that is being used in the Green-Senti proyect.

This ontology has been expanded to also integrate Landsat-8 data and integrated with the [AEMET meteorological ontology](http://aemet.linkeddata.es/), which has its own [ABox](aemet-ontology-abox).

SPARQL endpoint to query the data. URL: http://khaos.uma.es/opendata/sparql/

The Graph IRI is: http://khaos.uma.es/green-senti/earth-observation

### Sample SPARQL queries:

Some smaple SPARQL queries can be found [here.](http://opendata.khaos.uma.es/dataset/e3fe3669-5ecb-40a4-a785-6c7ac1b00be0/resource/5fb3051d-5a37-4124-9155-9b5567b02f95/download/sparql_samples.txt)

More information about the SPARQL query language can be found on the W3C SPARQL page https://www.w3.org/TR/sparql11-overview/
