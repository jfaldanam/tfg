
### Sample SPARQL queries:
The characters __X__ and __Y__ are used as variable that can be replaced.

##### List of all Scenes ######################################################

PREFIX greensenti: <http://khaos.uma.es/green-senti/earth-observation#>

SELECT ?location
WHERE {
    ?location rdf:type greensenti:Scene .
}
    
##### List of all dates that have products for a specific location (X) ########

PREFIX greensenti: <http://khaos.uma.es/green-senti/earth-observation#>
PREFIX time: <http://www.w3.org/2006/time#>

SELECT ?date
WHERE {
    ?date rdf:type time:GeneralDateTimeDescription .
    ?prod greensenti:hasDate ?date .
    ?prod greensenti:hasScene ?location . FILTER (?location = <X>) .
}

##### List all products in a specific location and date (X and Y) #############

PREFIX greensenti: <http://khaos.uma.es/green-senti/earth-observation#>
PREFIX time: <http://www.w3.org/2006/time#>

SELECT ?prod ?s ?p
WHERE { 
    ?prod ?s ?p .
    ?prod greensenti:hasScene ?location . FILTER (?location = <X>) .
    ?prod greensenti:hasDate ?date . FILTER (?date = <Y>) .
}

