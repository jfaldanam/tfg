Assertion component (ABox) of the [AEMET meteorological ontology.](http://aemet.linkeddata.es/)

SPARQL endpoint to query the data. URL: http://khaos.uma.es/opendata/sparql/

The Graph IRI is: http://aemet.linkeddata.es/ontology/

### Sample SPARQL queries:

Some smaple SPARQL queries can be found [here.](http://opendata.khaos.uma.es/dataset/7868beb5-c559-41c3-8334-2bcc714af4ed/resource/1e2eec39-9556-435f-bd90-c6c2f41ca144/download/sparql_samples.txt)

More information about the SPARQL query language can be found on the W3C SPARQL page https://www.w3.org/TR/sparql11-overview/
