Dataset containing the bands and their vegetative indices values for both Sentinel-2 and Landsat 8 products.

The only products used has been those where the difference between the capture time in Sentinel-2 and Landsat 8 is lower than 1 day.
