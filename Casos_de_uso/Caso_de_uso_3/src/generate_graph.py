import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt

def normalize(df):
    '''
    Normalizes the dataset received as parameter, except the column 'date'
    '''
    result = df.copy()
    result = result.drop('date', 1)
    for feature_name in result.columns:
        max_value = df[feature_name].max()
        min_value = df[feature_name].min()
        result[feature_name] = (df[feature_name] - min_value) / (max_value - min_value)
    return result

# NDVI vs. Temperature
data = pd.read_csv('NDVITemp.csv')
data = normalize(data)
sns.jointplot(data=data, x='temperatura', y='ndvi',
    kind='reg', xlim=(-0.05, 1.05), ylim=(-0.05, 1.05)).set_axis_labels("Temperature (ºC)", "NDVI")

plt.savefig('NDVITemp.png')

# MOISTURE vs. Temperature
data = pd.read_csv('MOISTURETemp.csv')
data = normalize(data)
plot = sns.jointplot(data=data, x='temperatura', y='moisture',
    kind='reg', xlim=(-0.05, 1.05), ylim=(-0.05, 1.05)).set_axis_labels("Temperature (ºC)", "MOISTURE")

plt.savefig('MOISTURETemp.png')