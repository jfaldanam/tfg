import numpy as np
import rasterio
import os
from os.path import join
from shapely.geometry import shape
from shapely.ops import transform
from rasterio import mask
from functools import partial
import pyproj
import json
from sentinelsat.sentinel import read_geojson
import glob
import matplotlib.pyplot as plt
from tqdm import tqdm


def project_shape(geom, scs: str = 'epsg:4326', dcs: str = 'epsg:32630'):
    """ Project a shape from a source coordinate system to another one.
    The source coordinate system can be obtain with `rasterio` as illustrated next:

    >>> import rasterio
    >>> print(rasterio.open('example.jp2').crs)

    This is useful when the geometry has its points in "normal" coordinate reference systems while the geoTiff/jp2 data
    is expressed in other coordinate system.

    :param geom: Geometry, e.g., [{'type': 'Polygon', 'coordinates': [[(1,2), (3,4), (5,6), (7,8), (9,10)]]}]
    :param scs: Source coordinate system.
    :param dcs: Destination coordinate system.
    """
    project = partial(
        pyproj.transform,
        pyproj.Proj(init=scs),
        pyproj.Proj(init=dcs))

    return transform(project, shape(geom))

def compute_cloud_percentage(b3, b4, b11, **kwargs):
    """ Computes cloud percentage of an image using b03 and b04 
    
    :param b3: Green band (i.e., B03 for Sentinel-2).
    :param b4: Red band (i.e.m B04 for Sentinel-2).
    :param b11: SWIR band 11 (i.e., B11 for Sentinel-2).
    """

    #Detect which elements are cloud based on Braaten-Cohen-Yang cloud detector
    bRatio = (b3 - 0.175) / (0.39 - 0.175)
    NGDR = (b3 - b4) / (b3 + b4)
    tau = kwargs.get('tau', 0.2)

    is_cloud = ((bRatio > 1) | ((bRatio > 0) & (NGDR>0))) & (b3 != 0) & (b11 > tau)
    
    #Calculate and return cloud percentage
    if np.count_nonzero(b3) != 0:
        cloud_percentage = np.count_nonzero(is_cloud) * 100 / np.count_nonzero(b3)
    else:
        cloud_percentage = 100
    return cloud_percentage

def compute_greenzone(ndvi, threshold):
    """ Compute greenzone area by NDVI using an arbitrary threshold

    :param ndvi: NDVI as array.
    :param threshold: Minimum value of NDVI that is considered greenzone.
    """

    is_greenzone = ndvi > threshold
    greenzone_area = (np.count_nonzero(is_greenzone) * 100)
    greenzone_percentage = greenzone_area / (ndvi.size - np.count_nonzero(np.isnan(ndvi)))

    return greenzone_area, round(greenzone_percentage, 2)

def compute_leafgreenzone(slavi, threshold):
    """ Compute greenzone area by SLAVI using an arbitrary threshold

    :param slavi: SLAVI as array.
    :param threshold: Minimum value of SLAVI that is considered greenzone.
    """

    is_greenzone = (slavi > threshold) & np.logical_not(np.isinf(slavi))
    greenzone_area = (np.count_nonzero(is_greenzone) * 100)
    greenzone_percentage = greenzone_area / (slavi.size - np.count_nonzero(np.isnan(slavi)))
    return greenzone_area, round(greenzone_percentage, 2)

def crop_by_shape(filename: str, geom):
    """ Crop the file `filename` with a polygon mask.

    :param filename: Input filename (jp2, tif).
    :param geom: Geometry.
    """
    # load the raster, mask it by the polygon and crop it
    with rasterio.open(filename) as src:
        out_image, out_transform = mask.mask(src, shapes=[geom], crop=True)
    
    return out_image

def compute_csv(date,aemet):
    """ Extracts metheorological data from aemet json

    :param date: Date to extract data.
    :param aemet: Filename for the aemet.json data.
    """
    result = []

    with open(aemet, 'r') as f:
        content = f.read()
        json_object = json.loads(content)
    
    found = False

    for obj in json_object:
        datejson = obj.get('fecha')        
        if date == datejson:
            result.append(float(obj.get('altitud').replace(',','.')))
            result.append(float(obj.get('tmed').replace(',','.')))
            result.append(float(obj.get('tmin').replace(',','.')))
            result.append(obj.get('horatmin'))
            result.append(float(obj.get('tmax').replace(',','.')))
            result.append(obj.get('horatmax'))
            result.append(float(obj.get('prec').replace(',','.')))
            result.append(obj.get('dir'))
            result.append(float(obj.get('velmedia').replace(',','.')))
            result.append(float(obj.get('racha').replace(',','.')))
            result.append(obj.get('horaracha'))
            result.append(float(obj.get('presMax').replace(',','.')))
            result.append(obj.get('horaPresMax'))
            result.append(float(obj.get('presMin').replace(',','.')))
            result.append(obj.get('horaPresMin'))

            found = True
            pass

    return result, found

if __name__ == '__main__':

    geojson = "teatinos.geojson"
    footprint = read_geojson(geojson)
    shape = project_shape(footprint['features'][0]['geometry'])

    product_folder = "/home/khaosdev-4/Software/ndvi/1C/PRODUCT/result"
    aemet = "/home/khaosdev-4/Software/ndvi/PRODUCT/aemet.json"

    # Open csv and write header
    csv = open('/home/khaosdev-4/Software/ndvi/1C/PRODUCT/area1C.csv', 'w+')
    csv.write('DATE;GREEN AREA;GREEN ZONE PERCENTAGE;' + 
                'CLOUD PERCENTAGE;MOISTURE(GVMI);SLAVI AREA;SLAVI PERCENTAGE;')
    csv.write('HEIGHT(m);AVGTEMP(C);MINTEMP(C);TMINHOUR;MAXTEMP(C);TMAXHOUR;RAINFALL(l/m2);' +
                 'WINDDIR;AVGWIND(km/h);WINDGUST(km/h);WINDGUSTHOUR(km/h);MAXPRESS(hPa);' +
                 'MAXPRESSHOUR;MINPRESS(hPa);MINPRESSHOUR\n')

    products= os.listdir(product_folder)
    for product in tqdm(products):
        print(product)
        # Load bands for cloud percentage
        green_path = join(product_folder,product,"B03.jp2")
        GREEN = crop_by_shape(green_path,shape)/10000
        red_path = join(product_folder,product,"B04.jp2")
        RED = crop_by_shape(red_path,shape)/10000
        swir_path = join(product_folder,product,"B11.jp2")
        SWIR = crop_by_shape(swir_path,shape)/10000
        SWIR = np.repeat(np.repeat(SWIR[0,:, :(-1)], 2, axis=0), 2, axis=1)
        cloud_percentage = compute_cloud_percentage(GREEN[0,:,:], RED[0,:,:], SWIR)
        
        if(cloud_percentage < 15 and cloud_percentage > 0):
            # Load bands for index calculation            
            rededge5_path = join(product_folder,product,"B05.jp2")
            REDEDGE5 = crop_by_shape(rededge5_path,shape)/10000
            REDEDGE5 = np.repeat(np.repeat(REDEDGE5[0,:, :(-1)], 2, axis=0), 2, axis=1)

            nir_path = join(product_folder,product,"B08.jp2")
            NIR = crop_by_shape(nir_path,shape)/10000

            water_path = join(product_folder,product,"B09.jp2")
            WATER = crop_by_shape(water_path,shape)/10000
            WATER = np.repeat(np.repeat(WATER[0,:, :], 6, axis=0), 6, axis=1)[:(-4),:(-8)]
            
            swir12_path = join(product_folder,product,"B12.jp2")
            SWIR12 = crop_by_shape(swir12_path,shape)/10000
            SWIR12 = np.repeat(np.repeat(SWIR12[0,:, :(-1)], 2, axis=0), 2, axis=1)
            
            # Calculate indexes
            gvmi = (WATER+0.1 -(SWIR12+0.02))/(WATER + 0.1 + SWIR12 + 0.02)

            slavi = (WATER/(REDEDGE5 + SWIR12))
            leaf_greenzone_area, leaf_greenzone_percentage = compute_leafgreenzone(slavi,0.5)
            
            ndvi = (NIR - RED) / (NIR + RED)
            greenzone_area, greenzone_percentage = compute_greenzone(ndvi,0.5)
            
            # Add metheorlogical data
            
            try:
                columns, found = compute_csv(product,aemet)
            except AttributeError:
                print('Missing data on ' + product)
                continue 

            if found:
            # Print values
                csv.write(product + ';' + str(greenzone_area) + ';' + str(greenzone_percentage)
                + ';' + str(cloud_percentage) + ';' + str(np.average(gvmi))
                + ';' + str(leaf_greenzone_area) + ';' + str(np.average(leaf_greenzone_percentage)) + ';')
                for key in range(len(columns)):
                    csv.write('' + str(columns[key]) + ';')
                csv.write('\n')
    

    