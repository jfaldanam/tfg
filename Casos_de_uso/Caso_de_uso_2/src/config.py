from os import path

basedir = path.dirname(path.abspath(__file__))
config = {
    'geojson': path.join(basedir, 'geojson', 'teatinos_rectangle.geojson'),
    'landsat_products': '/home/pealma/Satellite/Landsat8',
    'sentinel_products': '/home/pealma/Satellite/Sentinel2/2A/result',
    'csv': path.join(basedir, 'dataset/ndvi.csv')
}

# Sentinel 2 bands
S2_bands = {
    'B01': 'coastal',
    'B02': 'blue',
    'B03': 'green',
    'B04': 'red',
    'B05': 'rededge5',
    'B06': 'rededge6',
    'B07': 'rededge7',
    'B08': 'nir',
    'B8A': 'narrownir',
    'B09': 'water',
    'B11': 'swir11',
    'B12': 'swir12'
}

# Landsat 8 bands
L8_bands = {
    'B1': 'coastal',
    'B2': 'blue',
    'B3': 'green',
    'B4': 'red',
    'B5': 'nir',
    'B6': 'swir6',
    'B7': 'swir7',
    #'B8': 'pan',
    'B9': 'cirrus', 
    #'B10': 'LWI10',
    #'B11': 'LWI11'
}

# Sentinel 2 spatial resolution
S2_res = {
    'B01': '60',
    'B02': '10',
    'B03': '10',
    'B04': '10',
    'B05': '20',
    'B06': '20',
    'B07': '20',
    'B08': '10',
    'B8A': '20',
    'B09': '60',
    'B11': '20',
    'B12': '20'
}


# Landsat 8 spatial resolution
L8_res = {
    'B1': '30',
    'B2': '30',
    'B3': '30',
    'B4': '30',
    'B5': '30',
    'B6': '30',
    'B7': '30',
    #'B8': '15',
    'B9': '30',
    #'B10': '100',
    #'B11': '100'
}