from os.path import join
from functools import partial
import glob
import pyproj
from shapely.geometry import shape
from shapely.ops import transform
import numpy as np
import rasterio
from rasterio import mask
from matplotlib import pyplot as plt
from sentinelsat.sentinel import read_geojson
from affine import Affine
import math
from config import L8_bands, L8_res, S2_bands, S2_res, config
from datetime import datetime, timedelta

def nearest_date(items, goal):
    ''' Takes a dict of datetime objects and
    a goal date and returns the closest element
    and its timedelta'''
    dates = items.values()
    nearest=min(dates, key=lambda x: abs(x - goal))
    timedelta = abs(nearest - goal)

    result = [result for result, v in items.items() if v==nearest][0]
    return result, timedelta

def nearest_pairs(L8_folder, S2_folder, threshold:int =1):
    ''' Takes each satellite folder and return a
    dictionary with the following structure
    {
        'L8_product': ('S2_product', timedelta),
        ...
    }

    Returns for each Landsat product the nearest in
    date sentinel product, without passing a set threshold
    in days
    '''
    path = join(L8_folder, '*')
    L8_path = glob.glob(path)

    path = join(S2_folder, '*')
    S2_path = glob.glob(path)

    L8_dates = {}
    for product in L8_path:
        # Landsat 8 product naming convention LXS PPPRRR YYYYDDD  GSIVV
        product_name = product.split('/')[-1]
        date = datetime.strptime(product_name[9:-5], '%Y%j')
        L8_dates[product_name] =  date

    S2_dates = {}
    for product in S2_path:
        product_name = product.split('/')[-1]
        date = datetime.strptime(product_name, '%Y%m%d')
        S2_dates[product_name] = date
    
    result = {}
    for product in L8_dates.keys():
        tmp = nearest_date(S2_dates, L8_dates[product])
        if tmp[1] < timedelta(days=threshold):
            result[product] = tmp
    
    return result

def project_shape(geom, scs: str = 'epsg:4326', dcs: str = 'epsg:32630'):
    """ Project a shape from a source coordinate system to another one.
    The source coordinate system can be obtain with `rasterio` as illustrated next:

    >>> import rasterio
    >>> print(rasterio.open('example.jp2').crs)

    This is useful when the geometry has its points in "normal" coordinate reference systems while the geoTiff/jp2 data
    is expressed in other coordinate system..

    :param geom: Geometry, e.g., [{'type': 'Polygon', 'coordinates': [[(1,2), (3,4), (5,6), (7,8), (9,10)]]}]
    :param scs: Source coordinate system.
    :param dcs: Destination coordinate system.
    """
    project = partial(
        pyproj.transform,
        pyproj.Proj(init=scs),
        pyproj.Proj(init=dcs))

    return transform(project, shape(geom))

def crop_reflectance_teatinos(satelite:str, product:str, band:str, debug=False):
    """ Crop the file `filename` with a polygon mask.
    Images with spatial resolution = 10m are cropped by real shape of teatinos.
    Images with spatial resolution = 20m or 60m cover a rectangle bigger than teatinos avoiding data loss. 
    Tranformed to reflectance dividing by 10000, only for Sentinel-2 bands.
    :param filename: Input filename (jp2, tif). 
    """
    if satelite is 'L8':
        path = join(config['landsat_products'], product, '*' + band + '.TIF')
        path = glob.glob(path)[0]
        spatial_resolution = int(L8_res[band])
    elif satelite is 'S2':
        path = join(config['sentinel_products'], product, '*' + band + '.jp2')
        path = glob.glob(path)[0]
        spatial_resolution = int(S2_res[band])
    
    scale = spatial_resolution/10
    if debug:
        print('Cropping at', path, 'with resolution', spatial_resolution, ' and a scale factor of', scale)

    geojson = config['geojson']
    footprint = read_geojson(geojson)
    shape = project_shape(footprint['features'][0]['geometry'])

    # load the raster, mask it by the polygon and crop it
    with rasterio.open(path) as src:
        out_image, _ = mask.mask(src, shapes=[shape], crop=True)

    # Calculate reflectance depending on Satellite
    if satelite is 'L8':
        # Because of how the Landsat 8 reflectance are calculated
        # a zero mask is needed to set to zero again the zone outside
        # the scene
        zero_mask = np.where(out_image[0,:,:] == 0)
        out_image = ((out_image[0,:,:]*0.00002) - 0.1)*math.sin(32.80464684)
        out_image[zero_mask] = 0
    else: 
        out_image = out_image[0,:,:]/10000

    out_image = np.repeat(np.repeat(out_image, scale, axis=0), scale, axis=1)

    return out_image

def get_coordinates(satelite:str, product:str, band:str):
    geojson = config['geojson']
    if satelite is 'L8':
        path = join(config['landsat_products'], product, '*' + band + '.TIF')
        path = glob.glob(path)[0]
    elif satelite is 'S2':
        path = join(config['sentinel_products'], product, '*' + band + '.jp2')
        path = glob.glob(path)[0]

    with rasterio.open(path) as src:
        _, coordinates = mask.mask(src, shapes=[project_shape(read_geojson(geojson)['features'][0]['geometry'])], crop=True)
    
    return coordinates

def compute_ndvi(red, nir, output_directory: str, output_filename: str = 'NDVI', fmt=('png', 'tiff'), dpi=300):
    """ Compute Normalized Difference Vegetation Index (NDVI).

    ..note:: https://eos.com/index-stack/
    ..note:: https://medium.com/analytics-vidhya/satellite-imagery-analysis-with-python-3f8ccf8a7c32

    :param red: Red band (i.e., B04 for Sentinel-2).
    :param nir: NIR band (i.e., B08 for Sentinel-2).
    :param output_directory: Output directory path.
    :param output_filename: Output image filename.
    :param fmt: Output format(s) as list.
    :param dpi: Output image DPI.
    """

    # compute the index
    ndvi = (nir - red) / (nir + red)
    vmin, vmax = np.nanpercentile(ndvi, (1, 99))  # 1-99% contrast stretch

    # plot image
    fig, ax = plt.subplots(figsize=plt.figaspect(ndvi), frameon=False)
    fig.subplots_adjust(0, 0, 1, 1)

    ax.imshow(ndvi, cmap='RdYlGn', vmin=vmin, vmax=vmax)

    # export in multiple formats
    for f in fmt:
        filename = join(output_directory, '{}.{}'.format(output_filename, f))
        fig.savefig(filename, dpi=dpi, facecolor='#f8f9fa', transparent=True, edgecolor='none')

    # close figure to avoid overflow
    plt.close(fig)

    return ndvi

def compute_cvi(green, red, nir, output_directory: str, output_filename: str = 'CVI', fmt=('png', 'tiff'), dpi=300):
    """ Chlorophyll vegetation index (CVI).

    ..note:: https://www.indexdatabase.de/db/i-single.php?id=391
    
    :param green: Green band (i.e., B03 for Sentinel-2).)
    :param red: Red band (i.e., B04 for Sentinel-2).
    :param nir: NIR band (i.e., B08 for Sentinel-2).
    :param output_directory: Output directory path.
    :param output_filename: Output image filename.
    :param fmt: Output format(s) as list.
    :param dpi: Output image DPI.
    """

    # compute the index
    cvi = (nir * red) / np.square(green)
    vmin, vmax = np.nanpercentile(cvi, (1, 99))  # 1-99% contrast stretch

    # plot image
    fig, ax = plt.subplots(figsize=plt.figaspect(cvi), frameon=False)
    fig.subplots_adjust(0, 0, 1, 1)

    ax.imshow(cvi, cmap='RdYlGn', vmin=vmin, vmax=vmax)

    # export in multiple formats
    for f in fmt:
        filename = join(output_directory, '{}.{}'.format(output_filename, f))
        fig.savefig(filename, dpi=dpi, facecolor='#f8f9fa', transparent=True, edgecolor='none')

    # close figure to avoid overflow
    plt.close(fig)

    return cvi

def compute_fvc(blue, green, red, output_directory: str, output_filename: str = 'FVC', fmt=('png', 'tiff'), dpi=300):
    """ Fractional Vegetation Cover (FVC).

    ..note:: https://www.researchgate.net/publication/228898521_Fractional_vegetation_cover_estimation_from_PROBACHRIS_data_Methods_analysis_of_angular_effects_and_application_to_the_land_surface_emissivity_retrieval

    :param blue: Blue band (i.e., B02 for Sentinel-2).
    :param green: Green band (i.e., B03 for Sentinel-2).)
    :param red: Red band (i.e., B04 for Sentinel-2).
    :param output_directory: Output directory path.
    :param output_filename: Output image filename.
    :param fmt: Output format(s) as list.
    :param dpi: Output image DPI.
    """

    # compute the index
    vari = (green - red) / (green + red - blue)
    fvc = (84.75 * vari) - 22.78
    vmin, vmax = np.nanpercentile(fvc, (1, 99))  # 1-99% contrast stretch

    # plot image
    fig, ax = plt.subplots(figsize=plt.figaspect(fvc), frameon=False)
    fig.subplots_adjust(0, 0, 1, 1)

    ax.imshow(fvc, cmap='RdYlGn', vmin=vmin, vmax=vmax)

    # export in multiple formats
    for f in fmt:
        filename = join(output_directory, '{}.{}'.format(output_filename, f))
        fig.savefig(filename, dpi=dpi, facecolor='#f8f9fa', transparent=True, edgecolor='none')

    # close figure to avoid overflow
    plt.close(fig)

    return fvc
