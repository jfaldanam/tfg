import config
import utils
import sys
import os

from shapely.ops import transform
import pyproj
import numpy as np
from functools import partial
from shapely.geometry import Point

from matplotlib import pyplot as plt

def calculate_product(L8_product, S2_product, csv, index=1, debug=False):
    '''Given a Landsat and a sentinel poducts calculates several indexes
    and stores the band and index value for each pixel on `csv`
    with `debug` set to true it plots all bands and indexes'''
    L8 = {}
    for key in config.L8_bands.keys():
        L8[key] = utils.crop_reflectance_teatinos('L8', L8_product, key, debug=debug)
    S2 = {}
    for key in config.S2_bands.keys():
        S2[key] = utils.crop_reflectance_teatinos('S2', S2_product, key)

    coordinates = utils.get_coordinates('S2', S2_product, 'B04')
    
    example_band = S2['B03']

    rows, columns = example_band.shape
    project = partial(
        pyproj.transform,
        pyproj.Proj(init='epsg:32630'),
        pyproj.Proj(init='epsg:4326'))

    #Compute epsg:4326 coordinates of top-left and right-bottom pixels
    initial_long, initial_lat = transform(project, Point(coordinates * (0, 0))).bounds[0:2]
    final_long, final_lat = transform(project, Point(coordinates * (example_band.shape[1]-1, example_band.shape[0]-1))).bounds[0:2]
    pixel_lat = (final_lat-initial_lat)/rows
    pixel_long = (final_long-initial_long)/columns
    longitude = initial_long
    if debug:
        print('Product coordinates')
        print('Initial latitude: ' + initial_lat)
        print('Initial longitude: ' + initial_long)
        print('Final latitude: ' + final_lat)
        print('Final longitude: ' + final_long)

    folder = 'index/' + str(index)
    os.makedirs(folder, exist_ok=True)


    # Compute indixes
    L8_ndvi = utils.compute_ndvi(L8['B4'], L8['B5'], folder, L8_product + 'NDVI')
    S2_ndvi = utils.compute_ndvi(S2['B04'], S2['B08'], folder, S2_product + 'NDVI')
    L8_ndvi[np.isnan(L8_ndvi)] = 0
    S2_ndvi[np.isnan(S2_ndvi)] = 0

    L8_cvi = utils.compute_cvi(L8['B3'], L8['B4'], L8['B5'], folder, L8_product + 'CVI')
    S2_cvi = utils.compute_cvi(S2['B03'], S2['B04'], S2['B08'], folder, S2_product + 'CVI')
    L8_cvi[np.isnan(L8_cvi)] = 0
    S2_cvi[np.isnan(S2_cvi)] = 0

    L8_fvc = utils.compute_fvc(L8['B2'], L8['B3'], L8['B4'], folder, L8_product + 'FVC')
    S2_fvc = utils.compute_fvc(S2['B02'], S2['B03'], S2['B04'], folder, S2_product + 'FVC')
    L8_fvc[np.isnan(L8_fvc)] = 0
    S2_fvc[np.isnan(S2_fvc)] = 0

    if debug:
        # Show L8 bands
        fig = plt.figure()
        fig.canvas.set_window_title('Landsat-8 bands')
        for key, i in enumerate(S2.keys()):
            sub1 = fig.add_subplot(4, 3, i)
            sub1.set_title(config.S2_bands[key] + ': ' + config.S2_res[key] + 'm')
            sub1.imshow(S2[key])

        # Show S2 bands
        fig = plt.figure()
        fig.canvas.set_window_title('Sentinel-2 bands')
        for key, i in enumerate(L8.keys()):
            sub1 = fig.add_subplot(3, 3, i)
            sub1.set_title(config.L8_bands[key] + ': ' + config.L8_res[key] + 'm')
            sub1.imshow(L8[key])

        # Show indexes
        fig = plt.figure()
        fig.canvas.set_window_title('Indexes')
        sub1 = fig.add_subplot(2, 3, 1)
        sub1.set_title('L8 NDVI: 30m')
        sub1.imshow(L8_ndvi)

        sub1 = fig.add_subplot(2, 3, 2)
        sub1.set_title('L8 CVI: 30m')
        sub1.imshow(L8_cvi)

        sub1 = fig.add_subplot(2, 3, 3)
        sub1.set_title('L8 FVC: 30m')
        sub1.imshow(L8_fvc)

        sub1 = fig.add_subplot(2, 3, 4)
        sub1.set_title('S2 NDVI: 10m')
        sub1.imshow(S2_ndvi)

        sub1 = fig.add_subplot(2, 3, 5)
        sub1.set_title('S2 CVI: 10m')
        sub1.imshow(S2_cvi)

        sub1 = fig.add_subplot(2, 3, 6)
        sub1.set_title('S2 FVC: 10m')
        sub1.imshow(S2_fvc)

        plt.show()     

    for c in range(columns):
        latitude = initial_lat
        for r in range(rows):
            line = '{}; {}; {}; {}'.format(r, c, latitude, longitude)
            
            for key in S2.keys():
                line += '; {}'.format(S2[key][r, c])

            for key in L8.keys():
                line += '; {}'.format(L8[key][r, c])

            line += '; {}; {}; {}; {}; {}; {}'.format(S2_ndvi[r, c], L8_ndvi[r, c], S2_cvi[r, c], L8_cvi[r, c], S2_fvc[r, c], L8_fvc[r, c])

            #Write CSV
            line += '\n'
            csv.write(line)
                
            latitude += pixel_lat
        longitude += pixel_long

if __name__ == '__main__':

    # Argument handler
    debug = False
    max_delta = 1
    for i, arg in enumerate(sys.argv):
        if arg == '-d':
            print('Debug mode enabled')
            debug = True
        if arg == '-l':
            max_delta = int(sys.argv[i + 1])

    header = 'pixel_x; pixel_y; lat; long; '
    for _, band in config.S2_bands.items():
        header += 'S2_' + band + '; '
    for _, band in config.L8_bands.items():
        header += 'L8_' + band + '; '
    header += 'S2_NDVI; L8_NDVI; S2_CVI; L8_CVI; S2_FVC; L8_FVC'

    with open(config.config['csv'], 'w+') as csv:
        header += '\n'
        csv.write(header)
        pairs = utils.nearest_pairs(config.config['landsat_products'], config.config['sentinel_products'], threshold=max_delta)
        print('Found {} products with a time delta < {} days'.format(len(pairs), max_delta))
        print('\tLandsat-8\t\tSentinel-2\tTime delta')
        for landsat, value in pairs.items():
            sentinel, delta = value
            print('\t{}\t{}\t{}'.format(landsat, sentinel, delta))

        index = 0
        for landsat, value in pairs.items():
            sentinel, delta = value
            calculate_product(landsat, sentinel, csv, index, debug)
            index +=1
