# Casos de uso

Esta carpeta incluye tanto el código como los resultados de cada Caso de uso.

Caso de uso 1: Fusión de Sentinel-1 vs Sentinel-2

Caso de uso 2: Comparación de valores de NDVI entre Sentinel-2 y Landsat 8

Caso de uso 3: Correlaciones entre índices de vegetación y condiciones climatológicas