\subsection{Web Semántica}

La Web Semántica \citep{Berners2001} no es una web separada, sino una extensión de la actual, en la que se le da a la información un significado bien definido, mejorando la cooperación entre ordenadores y personas para trabajar juntos. El desarrollo en este campo está aumentando significativamente nuevas funcionalidades gracias a que los ordenadores serán capaces de procesar y ``entender'' los datos que en la actualidad solo representan.

El termino ``Web Semántica'' \citepalias{W3C2015} se refiere a la visión del World Wide Web Consortium (W3C) sobre una red de datos enlazados, cuyo objetivo final es permitir a los ordenadores hacer más trabajo útil mediante tecnologías que permitan la creación de almacenes de datos para la red, la construcción de vocabularios y la escritura de reglas para manejar los datos.

Para hacer esta red de datos una realidad, es necesario la existencia de una gran cantidad de datos disponibles con un formato estándar,que además puedan obtenerse y gestionarse por otras herramientas de la Web Semántica. Estos repositorios de datos deben incluir relaciones entre los datos que contienen y con otros datos disponibles en la web. A esta colección de conjuntos de datos interrelacionados es a lo que se conoce como Datos Enlazados (Linked Data) \citepalias{W3CLinkedData2015}.

Algunas de las tecnologías mas importantes en el campo de la Web Semántica y de los Linked Data son RDF, OWL y SPARQL, como podemos observar en la Figura \ref{fig:SemanticStack}, que pasamos a definir a continuación.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.85\linewidth]{img/SemanticWebStack.png}
    \caption{Stack de tecnologías de la Web Semántica. Imagen original de dominio público.}
    \label{fig:SemanticStack}
\end{figure}

\subsubsection*{Resource Description Framework (RDF)}
RDF \citep{Schreiber2014} es un estándar del W3C para la representación de información sobre recursos en la Web. Situaciones en las que se recomienda el uso de RDF son las aplicaciones en las que los datos van a ser procesados por otras aplicaciones en vez de ser solo mostrados a personas. RDF ofrece un marco de trabajo común con el que expresar información que puede ser intercambiada entre aplicaciones sin perder el significado, donde cada recurso Web esta identificado por una URI (Uniform Resource Identifier).

RDF puede estar serializado en varios formatos, siendo algunos de los más populares Turtle, N-Triples o RDF/XML. Independientemente de la serialización utilizada, las tripletas siguen el formato.
\begin{lstlisting}
    <Sujeto> <Predicado> <Objeto>
\end{lstlisting}

En este proyecto, para trabajar con RDF se ha utilizado la librería de código abierto rdflib
\footnote{rdflib: \url{https://github.com/RDFLib/rdflib}}
para Python 3.

\subsubsection*{Ontology Web Language (OWL)}
Una ontología define un conjunto de primitivas de representación con las que modelar un dominio de conocimiento. Las primitivas de representación suelen ser clases (o conjuntos), atributos (o propiedades) y relaciones. La definición de las primitivas incluye información sobre su significado y restricciones de forma coherente con su aplicación. Las ontologías se sitúan en un nivel ``semántico'', como abstracciones de los modelos de datos. Debido a esta independencia, las ontologías son usadas para la integración de bases de datos heterogéneas y la especificación de interfaces para servicios independientes basados en conocimiento \citep{Gruber2009}.

El Lenguaje de Ontología Web (OWL) \citepalias{W3COWL2004} es un lenguaje de marcado semántico para publicar y compartir ontologías en Web. OWL fue desarrollado y publicado en 2004 como una extensión de vocabulario de RDF y derivado del lenguaje de ontologías DAML+OIL.

OWL 2 \citepalias{W3COWL2012} es una extensión y revisión del Lenguaje de Ontología Web. Publicado en 2009 por la W3C, diseñado para la representación de conocimiento sobre entidades,  conjuntos de entidades y las relaciones entre entidades, en forma de ontologías.

OWL 2 proporciona clases, individuos (instancias de clases), propiedades y valores para dichas propiedades para definir ontologías. Estas ontologías son normalmente intercambiadas como documentos RDF.

Para el desarrollo de la ontología en OWL 2 se ha utilizado la herramienta Protégé.
\footnote{Protégé: \url{https://protege.stanford.edu/}}.

\subsubsection*{SPARQL Protocol And RDF Query Language (SPARQL)}
SPARQL \citep{Harris2013} es un lenguaje de consultas para grafos RDF, que permite también la ejecución de consultas entre varios grafos que pueden estar en repositorios distintos de la web (consultas federadas).

Las consultas SPARQL pueden estar formadas por las siguientes clausulas principales:
\begin{itemize}
    \item SELECT: Devuelve todas o un subconjunto de las variables que coincidan con el patrón.
    \item CONSTRUCT: Devuelve un grafo RDF construido al sustituir las variables en una plantilla de tripletas.
    \item ASK: Devuelve un valor (TRUE o FALSE) indicando si hay tripletas que coincidan con el patrón.
    \item DESCRIBE: Devuelve un grafo RDF que describe los recursos encontrados.
    \item INSERT: Añade tripletas a un grafo.
    \item DELETE: Elimina tripletas de un grafo.
\end{itemize}

Estas clausulas principales vienen acompañadas de una clausula WHERE formada por un conjunto de tripletas o patrones de tripletas, que se utilizaran en la consulta sobre los grafos que se especifiquen.

Las consultas utiliza el carácter \texttt{?X} para indicar variables, donde X es el nombre de la variable, y prefijos para indicar a que grafo pertenece el recurso sin tener que escribir la URI completa, por ejemplo en la tripleta

\texttt{?prod greensenti:hasScene greensenti:ejido .}

\texttt{?prod} es una variable en la que se almacenan productos que estén relacionado por un recurso \textit{hasSecene}, definido en un grafo identificado como \texttt{greensenti}, a un recurso \texttt{ejido} del mismo grafo.

Un ejemplo de SPARQL sería la consulta \ref{listing:ExampleSPARQL}, que nos devolvería todos los productos (?prod), que incluyan la localización del campus universitario del Ejido, ordenados por fecha de forma descendiente.
\begin{listing}[H]
    \begin{minted}
[frame=lines, framesep=2mm, baselinestretch=1.2, fontsize=\footnotesize]{sparql}
PREFIX greensenti: <http://khaos.uma.es/green-senti/earth-observation#>

SELECT ?prod 
WHERE {
    ?prod greensenti:hasDate ?date .
    ?prod greensenti:hasScene greensenti:ejido .
} ORDER BY DESC(?date)
    \end{minted}
    \caption{Consulta de ejemplo sobre SPARQL}
    \label{listing:ExampleSPARQL}
\end{listing}
Para la ejecución de consultas SPARQL en repositorios RDF desde Python se ha utilizado la librería SPARQLWrapper. \footnote{SPARQLWrapper: \url{https://github.com/RDFLib/sparqlwrapper}}

\subsubsection*{Ontologías destacadas}
En esta sección se realiza una revisión a las ontologías actuales que destacan dentro de los campos de la Web Semántica y Remote Sensing. Estas ontologías han sido integradas con la ontología diseñada en este Trabajo de Fin de Grado, realizando un estudio e integración de acuerdo con los estándares de diseño Ontology 101 \citep{Ontology101}.
\begin{itemize}
\item AEMET

El objetivo de la red de ontologías AEMET \citepalias{AEMET} es la representación del conocimiento relacionado con las mediciones realizadas por la red de estaciones meteorológicas de la AEMET (Agencia Estatal de Meteorología). Estas mediciones representa el estado de la atmósfera en un instante y lugar concreto y se realizan por medio de los sensores integrados en cada estación meteorológica.

El prefijo utilizado en este documento para referirse a esta ontología es \textit{aemet}.

\item GeoSPARQL

El estándar GeoSPARQL \citepalias{GeoSPARQLStandar} de la OGC(Open Geospatial Consortium) soporta la representación y consulta de datos geoespáciales en la Web Semántica. GeoSPARQL define un vocabulario para la representación de datos geoespáciales en RDF y define una extensión del lenguaje de consultas SPARQL para procesar datos geoespáciales.

El prefijo utilizado en este documento para referirse a esta ontología es \textit{geo}.

\item OWL-Time

Ontología desarrollada en el contexto del World Wide Web Consortium (W3C) \citep{OWL-Time}. Esta ontología proporciona un vocabulario para expresar propiedades temporal como instantes o intervalos de tiempo. Permite la representación de los instantes temporales en distintos sistemas de referencia como el tiempo Unix o el calendario Gregoriano.

El prefijo utilizado en este documento para referirse a esta ontología es \textit{time}.
\end{itemize}

\subsection{Teledetección}
Teledetección (Remote Sensing) \citep{Schowengerdt2007} son las técnicas para conseguir información sobre una entidad sin tener contacto directo con ella, utilizando por ejemplo drones o satélites. Debido a la falta de contacto directo con el objeto de interés se utilizan señales propagadas por algún medio, por ejemplo señales ópticas o acústicas. Dentro de los campos de las Ciencias de la Tierra son muy utilizadas las técnicas de teledetección.

\subsubsection*{Programa Copérnico}
El programa Copérnico (anteriormente conocido como GMES o Global Monitoring for Environment and Security) es una iniciativa de la Comisión Europea (EC) en colaboración con la Agencia Espacial Europea (ESA) \citepalias{CopernicousOverview} para proporcionar información, de forma constante, precisa y de fácil acceso, para mejorar el estudio del medio ambiente y entender y mitigar los efectos del cambio climático y asegurar la seguridad civil.

Para ello, la ESA esta desarrollado la familia de satélites Sentinel, de los que actualmente hay 7 en orbita, diseñados para proporcionar imágenes ópticas de gran resolución. Estas imágenes son distribuidas en ``productos'' que incluyen tanto las imagenes, en las distintas espectrales, como sus metadatos.

El servicio Copernicus Open Access Hub \footnote{Copernicus Open Access Hub: \url{https://scihub.copernicus.eu/dhus/}} permite el filtro y descarga gratuita de los productos de todos los satélites de la familia Sentinel con tan solo registrarnos. En la Figura \ref{fig:copernicous} podemos ver un ejemplo de la herramienta en uso.

\begin{figure}
    \centering
    \includegraphics[width=0.90\linewidth]{img/scihub-copernicous.jpg}
    \caption{Servicio Copernicous Open Access Hub para la descarga de productos Sentinel \url{https://scihub.copernicus.eu/dhus/}.}
    \label{fig:copernicous}
\end{figure}

\subsubsection*{Productos de datos multiespectrales de Sentinel-2}
Las imágenes de los satélites de la familia Sentinel son distribuidas en productos. Los productos son un archivo comprimido en formato .zip, e incluyen además de las imágenes todos los metadatos que podemos necesitar para trabajar con ellas. Los productos Sentinel son distribuidos en varios niveles de procesado, los niveles 1-C y 2-A son los únicos distribuidos de forma abierta. Las diferencias entre ambos niveles pueden verse en la Tabla \ref{fig:productLevel}.

\begin{table}[H]
    \centering
    \begin{tabular}{|l|l|l|l|}
    \hline
        Name  & High-level Description & Data Volume \\ \hline
        Level-1C  & Top-of-atmosphere reflectances & 600 MB (each 100x100 km2) \\
        & in cartographic geometry & \\ \hline
        Level-2A  & Bottom-of-atmosphere reflectance & 800 MB (each 100x100 km2) \\
        & in cartographic geometry & \\ \hline
    \end{tabular}
    \caption{Tipos de productos para el Sentinel-2 \url{https://sentinel.esa.int/web/sentinel/user-guides/sentinel-2-msi/product-types}}
    \label{fig:productLevel}
\end{table}

El tipo de reflectancia se refiere a en que punto de la atmósfera ha sido tomado un dato. La reflectancia TOA (Top-of-atmosphere) es el valor del dato tomado por el sensor en un satélite, mientras que la reflectancia BOA (Bottom-of-atmosphere) es el valor que tendría si el satélite estuviera bajo la atmósfera, y por lo tanto el valor no estuviera distorsionado por ella.

En este trabajo se hará uso de los productos de nivel 2-A ya que utiliza la reflectancia BOA que nos ofrece un valor más correcto, aunque se puede convertir de uno a otro mediante ciertas formulas más complejas.

\subsubsection*{Índices de vegetación}
Un índice de vegetación \citep{Bannari1996} es un valor calculado a partir de un conjunto de canales o bandas de sensores de satélite, y cuantifica la intensidad de un fenómeno que es muy complejo para ser descompuesto en parámetros conocidos.

Un ejemplo de este tipo de índice es el Índice de vegetación de diferencia normalizada (NDVI), que cuantifica si el objetivo contiene vegetación verde o no. El NDVI se calcula $NDVI = \frac{NIR - RED}{NIR + RED}$, donde $RED$ es el canal rojo y $NIR$ es el canal infrarrojo de espectro electromagnético. Si utilizamos las bandas del Sentinel-2 tendríamos que $NDVI = \frac{B08 - B04}{B08 + B04}$, donde $B04$ es la banda roja y $B08$ es la banda infrarroja.

Como podemos apreciar en la Figura \ref{fig:TCvsNDVI}, las zonas verdes destacan más en su valor NDVI, mientras que, por ejemplo, el campo de fútbol de césped artificial no sale destacado.

\begin{figure}[H]
  \centering
  \begin{subfigure}[b]{0.55\linewidth}
    \includegraphics[width=\linewidth]{img/TrueColorTeatinos.png}
  \end{subfigure}
  \begin{subfigure}[b]{0.55\linewidth}
    \includegraphics[width=\linewidth]{img/NDVITeatinos.png}
  \end{subfigure}
  \caption{Comparación entre una imagen de Sentinel-2 y el NDVI calculado a partir de ella. Imágenes proporcionadas por el proyecto Green-Senti en \url{https://khaos.uma.es/green-senti}}
  \label{fig:TCvsNDVI}
\end{figure}

\subsubsection*{Proyecto Green-Senti}
El proyecto Green-Senti del I Plan Propio Smart Campus UMA, propone un servicio web para la monitorización de las áreas verdes del campus de la Universidad de Málaga a través de la captura y análisis de imágenes de los satélites Sentinel-2 del Programa Copérnico \citepalias{GreenSenti}. El objetivo de este proyecto es proporcionar una solución novedosa que sirva de apoyo para mejorar la calidad ambiental del campus.

Este Trabajo de Fin de Grado se enmarca dentro de este proyecto.

\subsubsection*{Geographic Information System (GIS)}
Un sistema de información geográfica (GIS) es un sistema que permite el análisis, manipulación, organización y presentación de datos espaciales o geográficos, por ejemplo los obtenidos mediante técnicas de teledetección.

Para la visualización de productos del programa Copérnico y de los índices de vegetación calculados, se ha utilizado la herramienta QGIS. \footnote{QGIS: \url{https://www.qgis.org/}}

\subsection{Estado del Arte}

El desarrollo de técnicas basadas en conocimiento ha sido identificado como una de las direcciones más importantes para la investigación por la comunidad del Remote Sensing \citep{CHEN20163}, teniendo una tendencia significativa la investigación en el sub-campo del análisis de imágenes basado en objetos geográficos (GEOBIA). GEOBIA hace posible la clasificación de objetos en imágenes, es decir grupos de pixeles que comparten una serie de propiedades, en imágenes de satélites a partir de procedimientos de análisis basados en conocimiento de expertos \citep{BLASCHKE20102}. Debido a que detecta complejas relaciones topológicas, incluyendo información relacionada con la forma y la textura del objeto,  GEOBIA genera mejores resultados que otras técnicas basadas en el análisis por píxeles \citep{WEIH2010}, siendo GEOBIA reconocida por los expertos como un nuevo paradigma para el campo de Remote Sensing \citep{BLASCHKE2014180}.

Considerando los recientes avances en el procesamiento de imágenes con técnicas Big Data, los expertos consideran que un enfoque híbrido entre técnicas basadas en datos y técnicas basadas en conocimiento, combinando procesos inductivos y deductivos, será la mejor manera de aprovechar toda la capacidad de las aplicaciones de Remote Sensing \citep{Arvor2019}. Mientras que se realizan importantes esfuerzos en compartir mapas de cobertura terrestre, tanto globales como regionales \citep{Grekouskis2015} o en compartir mejores metodos y algoritmos para el procesamiento de imagenes, dentro del campo de Remote Sensing se dedica aún poco esfuerzo a formalizar, agregar y compartir el conocimiento de los expertos. En este contexto, técnicas de representación de conocimiento como las ontologías, representan una posible solución a este problema \citep{ARVOR2013125}.

El objetivo de este Trabajo de Fin de Grado es generar una ontología en formato OWL-2, llamada earth-observation.owl, que pueda ser utilizada para mejorar la interpretación de las imágenes de satélites tal y como es descrito en \citep{Arvor2019} y que sirva de nexo de unión e integración de aquellas ontologías existentes en el capo de las técnicas de sensores remotos orientados a observación de la Tierra.

\subsection{Otras tecnologías utilizadas}
Para el desarrollo del Software implementado, en este Trabajo de Fin de Grado se han utilizado las siguientes tecnologías, que ya estaban siendo utilizadas en el proyecto Green-Senti.

\subsubsection*{Python}
Python es un lenguaje de programación de propósito general de alto nivel y código abierto, publicado por Guido van Rossum en 1991 \citepalias{Python}.

Para el desarrollo se ha usado la versión 3.7, junto con las siguientes librerías:
\begin{itemize}
    \item rdflib: Permite la creación y manipulación de grafos RDF desde Python de forma sencilla. \url{https://github.com/RDFLib/rdflib}
    \item SPARQLWrapper: Librería para la realización de consultas SPARQL. \url{https://github.com/RDFLib/sparqlwrapper}
    \item pymongo: Librería oficial para conexiones con MongoDB. \url{https://github.com/mongodb/mongo-python-driver}
    \item tqdm: Librería ligera y sencilla para mostrar barras de progreso a partir de iterables en Python. \url{https://github.com/tqdm/tqdm}
    \item requests: Librería sencilla para trabajar con peticiones HTTP desde Python. \url{https://github.com/psf/requests}
\end{itemize}

\subsubsection*{MongoDB}
MongoDB \footnote{MongoDB: \url{https://www.mongodb.com/}} es una base de datos distribuida NoSQL basada en documentos JSON. Algunas de las características que nos ofrece MongoDB son:

\begin{itemize}
    \item permite la indexación los documentos por cualquier campo.
    \item Replicación para ofrecer alta disponibilidad.
    \item Equilibrado de carga para escalar de forma horizontal nuestra base de datos.
    \item Framework de agregación para el procesamiento por lotes de datos.
\end{itemize}

Un documento valido de MongoDB podría ser:

\begin{listing}[H]
    \begin{minted}
[frame=lines, framesep=2mm, baselinestretch=1.2, fontsize=\footnotesize]{json}
    {
        "_id": "5cg0345jk4n6f2g",
        "productId": "134003f0-4bb4-4d71-80be-a60a6e75aec2",
        "metadata": {
            "orbitdirection": "DESCENDING",
            "ingestiondate": "2018-08-07 18:55"
        }
    }
    \end{minted}
    \caption{Ejemplo de un documento valido de MongoDB.}
    \label{listing:jsonExample}
\end{listing}

El proyecto Green-Senti utiliza una base de datos MongoDB para almacenar todos los metadatos de los productos de Sentinel analizados. Siendo esta base de datos la fuente de datos para el script de conversión a tripletas RDF, mediante la librería de Python pymongo \footnote{pymongo: \url{https://github.com/mongodb/mongo-python-driver}}. De esta forma, se hacen una serie de funciones mapping que siguen el esquema definido en la ontología.

\subsubsection*{Almacenamiento de tripletas RDF}
Virtuoso \footnote{Virtuoso: \url{https://virtuoso.openlinksw.com/}} es una de las base de datos RDF más extendidas, especialemente dentro del Open Data, que además ofrece la posibilidad de publicar un endpoint para consultas SPARQL.

Stardog \footnote{Stardog: \url{https://www.stardog.com/}} es una base de datos RDF, su última versión abandona el soporte de un endpoint publico, pero nos permite utilizar razonadores directamente sobre las tripletas RDF de los datos almacenados.

Stardog es la base de datos que ha sido usada para almacenar todas las tripletas RDF generadas a partir de los productos de Sentinel-2 durante el desarrollo, pero tras la finalización del proyecto se migraron los datos a Virtuoso para su publicación.

\subsection*{CKAN}
CKAN (Comprehensive Knowledge Archive Network) \footnote{CKAN: \url{https://ckan.org/}} es un portal open-source para el almacenamiento y distribución de Open Data y es mantenido por la Open Knowledge Foundation.

Los resultados de este Trabajo de Fin de Grado serán publicados en el CKAN del grupo de investigación Khaos dentro de la organización del proyecto Green-Senti \footnote{CKAN del proyecto Green-Senti: \url{https://opendata.khaos.uma.es/organization/green-senti}}.

En concreto se realizaran 5 repositorios:
\begin{itemize}
    \item \textbf{Earth Observation ontology}: Incluye la ontología asi como su ABox, en formato RDF/XML y nt, y consultas SPARQL de ejemplo.
    \item \textbf{AEMET ontology ABox}: Incluye la ABox generada a partir del portal Open-Data de AEMET, en formato RDF/XML y nt, y consultas SPARQL de ejemplo.
    \item \textbf{Green Area Teatinos}: Contiene los resultados del caso de uso 1 en formato CSV.
    \item \textbf{Comparison of several vegetative indices between Sentinel-2 and Landsat 8 products}: Contiene los resultados del caso de uso 2 en formato CSV.
    \item \textbf{Correlations between vegetation indices and weather conditions }: Contiene los resultados del caso de uso 3 en formato CSV.
\end{itemize}