\subsection{Contexto y problema}

El desarrollo de nuevos sensores y la creciente facilidad para acceder a los datos generados mediante técnicas de detección remota están transformando el campo del Remote Sensing \citep{Schowengerdt2007}. La investigación guiada por datos y el desarrollo de nuevos algoritmos innovadores para su procesamiento y las mejoras en las capacidades computacionales, sigue teniendo una gran importancia para el procesamiento de las grandes cantidades de datos generadas mediante los sistemas de satélites de Observación de la Tierra. Esta gran cantidad de datos es imprescindible para aplicaciones como la supervisión de zonas para controlar el cambio climático, la agricultura de precisión, etc. La comunidad del Remote Sensing considera que una de las lineas de investigación más prometedoras por su impacto práctico se encuentra en el desarrollo de nuevas técnicas guiadas por conocimiento, utilizando técnicas de representación del conocimiento como las ontologías \citep{Arvor2019}. El gran volumen y variedad de datos en este campo de investigación requiere de su consolidación, integración y enlazado, tanto de datos como de metadatos, que pueden provenir de distintas fuentes como, por ejemplo, productos de imágenes de satélites o drones (UAV).

El proyecto Green-Senti del I Plan Propio Smart Campus UMA, proporciona un servicio web para la monitorización de las zonas verdes del campus de la Universidad de Málaga, a través de la captación y análisis de imágenes de los satélites Sentinel-1/2/3 del programa Copérnico\footnote{Programa Copérnico: \url{https://www.copernicus.eu}}. Una de las aportaciones principales de este Trabajo de Fin de Grado al proyecto es una ontología descrita mediante Ontology Web Language (OWL) \citepalias{W3COWL2004}, que sirva como esquema de integración y consolidación de los datos, y que permitirá publicarlos como Linked Open Data y almacenarlos en repositorios en formato Resource Description Framework (RDF) \citep{Schreiber2014} para que puedan ser consultados mediante consultas avanzadas en el lenguaje de consulta de grafos SPARQL \citep{Harris2013}. 

\subsection{Motivación}

En el campo de Remote Sensing se está impulsando la investigación en el uso de ontologías como soporte a la integración, etiquetado e interpretación de la gran cantidad de datos que se producen. No obstante, aunque hay intentos de ontologías dispersas, no se encuentran lo suficientemente maduras, y no contemplan aspectos importantes como los índices de vegetación, que no son más que prototipos locales para ilustrar el potencial del uso de ontologías. \citep{Arvor2019}. Existen gran cantidad de constelaciones de satélites de Observación de la Tierra y UAVs (Vehículos aéreos no tripulados) que producen información sobre los mismos escenarios. Esta información debe poder ser integrada y dotada de soporte semántico.

En base a esta necesidad, en este Trabajo de Fin de Grado abordamos el diseño e implementación de un modelo semántico descrito mediante una ontología OWL que pueda ser utilizado para la consolidación, integración, enlazado y razonamiento sobre datos adquiridos por satélites que de soporte a análisis avanzados que no han sido posibles hasta el momento. Nos centraremos en la familia de satélites Sentinel, concretamente en Sentinel-2, aunque considerando otros importantes como Landsat o MODIS, y otros medios de captación como los UAVs.

\subsection{Objetivos}
El objetivo principal de este Trabajo de Fin de Grado es diseñar e implementar un modelo semántico guiado por una ontología OWL para la captación, consolidación, integración y enlazado de datos y metadatos de productos de imágenes multiespectrales de satélite para la observación de la tierra. Este modulo semántico proporciona una plataforma integrada para el enlazado y análisis Big Data enriquecido.

Para ello tiene como sub-objetivos:


\begin{enumerate}
    \item Diseño y desarrollo de una ontología OWL que especifique la estructura de la base de conocimiento y permita ser enlazada con otras ontologías.
    
    \item Transformación al estándar RDF, e integración con los datos de satélite previamente procesados, incluyendo los datos meteorológicos proporcionados por el portal de Open Data de AEMET, mediante el enlazado de una ontología ya desarrollada y publicada por AEMET.
    
    \item Desarrollo de una API de funciones y consultas avanzadas y federadas SPARQL para el acceso a los datos consolidados, que facilite un análisis avanzado, tanto de las imágenes como de los metadatos.
    
    \item Demostración basada en varios casos de uso reales de observación de la tierra.
\end{enumerate}

Para cumplir estos objetivos se presentaran los siguientes resultados:
\begin{itemize}
    \item Ontología OWL y repositorios RDF en Stardog que almacenarán tanto la TBox como la ABox \citep{Bergman09}, y permitirán el razonamiento semántico sobre las tripletas, tanto de los datos de satélite como los meteorológicos que publica la AEMET mediante su portal Open Data. Endpoint de acceso a los repositorios RDF: \url{http://khaos.uma.es/opendata/sparql/}.
    
    IRI(Internationalized Resource Identifier) de los grafos RDF para la realización de consultas SPARQL:
        \subitem earth-observation.owl: \url{http://khaos.uma.es/green-senti/earth-observation}
        \subitem aemet.owl: \url{http://aemet.linkeddata.es/ontology/}
        
    \item Repositorio de GitLab que contendrá los casos de uso, scripts y la ontología OWL. \url{https://gitlab.com/jfaldanam/tfg}
    \item Portal Open Data en CKAN para la publicación de los datos. \url{https://opendata.khaos.uma.es/organization/green-senti}
    \item API pública para realizar consultas SPARQL avanzadas sobre los datos RDF generados. Más información sobre el uso de este Endpoint puede ser encontrado en el CKAN. \url{http://khaos.uma.es/opendata/sparql/}
\end{itemize}

\subsection{Estructura del documento}
Esta memoria esta dividido en 6 Capítulos, recopilando toda la información relacionada con el desarrollo de este Trabajo de Fin de Grado. A continuación, se ofrece una breve descripción de cada uno de ellos.

En el Capítulo 2 se describe una revisión de los conceptos y tecnologías con los que estamos tratando, principalmente Web Semántica y Remote Sensing, profundizando en las tecnologías y herramientas software que han sido utilizadas en el desarrollo de este proyecto. Además, se presenta del estado del arte en los campos de la Web Semántica y Remote Sensing, mencionando otras ontologías que están siendo usadas en la actualidad.

En el Capítulo 3 se describe en detalle la propuesta realizada en este Trabajo de Fin de Grado y el Software implementado.

En el Capítulo 4 se muestran varios experimentos y casos de uso para demostrar la usabilidad y funcionamiento de la propuesta realizada.

En el Capítulo 5 se ofrece una discusión sobre la propuesta realizada en este Trabajo de Fin de Grado.

En el Capítulo 6 se presenta una conclusión al proyecto a la vez que se mencionaran posibles lineas de trabajo futuro como continuación de este Trabajo de Fin de Grado.