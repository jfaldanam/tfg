# Trabajo de Fin de Grado en Ingeniería del Software

## Resumen
En el campo del Remote Sensing se está empezando a investigar el uso de ontologías para la integración de la gran cantidad de datos que se producen. No obstante, todavía no se han generado ontologías ni modelos semánticos comúnmente aceptados que den soporte a dicha investigación.

El proyecto Green-Senti del I Plan Propio Smart Campus de la Universidad de Málaga requiere un modelo semántico que permita la consolidación, integración y enlazado de un gran volumen y variedad de datos derivados del análisis Big Data de productos de imágenes multiespectrales de satélite para la Observación de la Tierra, como por ejemplo, Sentinel-2.

Este Trabajo de Fin de Grado aborda, en primera instancia, una parte de esta necesidad a través de la propuesta de un modelo semántico definido mediante una ontología OWL que pueda ser utilizado para la consolidación, integración, razonamiento y enlazado de datos y metadatos de productos de imágenes multiespectrales del satélite Sentinel-2 del programa Copérnico de la Agencia Espacial Europea (ESA).

Con este objetivo se ha diseñado una ontología OWL y se han generado dos repositorios RDF en Stardog para almacenar las tripletas que representan todos los datos disponibles como parte del proyecto Green-Senti. Además se ha ampliado la API del proyecto Green-Senti para permitir consultas SPARQL a los repositorios RDF generados. Por ñultimo, se presentan casos de usos para validar la utilidad del módulo semántico propuesto.

**Palabras clave:** Web semántica, Datos Abiertos Enlazados, Lenguaje de Ontologías Web

## Abstract
In the field of Remote Sensing, the use of ontologies is being researched to integrate the large amount of data that is produced. However, no ontologies or semantic models have been accepted by the community to support this research.

The Green-Senti project of the ``I Plan Propio Smart Campus'' of the University of Málaga requires a semantic model that allows the consolidation, integration and linking of a large volume and variety of data derived from the Big Data analysis of multispectral satellite imagery products for the Earth Observation, for example, Sentinel-2.

This Final Year Dissertation addresses, in first instance, part of this need by proposing a semantic model defined by an OWL ontology that can be used for the consolidation, integration, reasoning and linking of data and metadata of multispectral image products of the Sentinel-2 satellite, part of the Copernicous programme of the European Space Agency (ESA).

With this objective an OWL ontology has been designed and two RDF repositories have been generated in Stardog to store the triplets that represent all the available data on the Green-Senti project. In addition, the Green-Senti project API has been extended to allow SPARQL queries to the generated RDF repositories. Several use cases are showcased to validate the utility of the proposed semantic module.

**Keywords:** Semantic web, Linked Open Data, Web Ontology Language


### License

The content of this project itself is licensed under the [Creative Commons Attribution-NonCommercial-NoDerivs 4.0 Generic License](http://creativecommons.org/licenses/by-nc-nd/4.0/) - see the [LICENSE](LICENSE) file for details, and the underlying source code used to format and display that content is licensed under the GNU GPLv3 license - see the [LICENSE.code](LICENSE.code) file for details.

[![cc-by-nc-nd](https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-nd/4.0/)

