# Ontología

Esta carpeta contiene:

* aemet.ttl: ABox para la ontología de [aemet.owl](http://aemet.linkeddata.es/ontology/).

* earth-observation.owl: Ontología OWL diseñada en este Trabajo de Fin de Grado.

* earth-observation.ttl: ABox para la ontología earth-observation.owl
